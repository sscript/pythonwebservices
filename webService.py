from flask import Flask
from flask import jsonify
from flask import request
app = Flask(__name__)
#planned objective for isMalware domains data set ,
# created an in-memory database in python using the dictionary data type.
urlInfoDB=[
 {
 'domain':'abc.com:80',
 'IP':'192.168.1.0',
 'isMalware':'true',
 'phishing/fraud':'trojan'
 },
 {
 'domain':'xyz.co.gs:80',
 'IP':'192.168.1.200',
 'isMalware':'true',
 'phishing/fraud':'trojan'
 },
  {
  'domain':'cisco.com:80',
  'IP':'192.168.1.200',
  'isMalware':'false',
  'phishing/fraud':'NA'
  }
 ]
 
#retrieve all domains using web services.
@app.route('/urlinfo/urls',methods=['GET'])
def getAllUrls():
    return jsonify({'urls':urlInfoDB})

# to retrieve the specific domain status about is Malware or not
@app.route('/urlinfo/urls/<hostnamePort>',methods=['GET'])
def getDomain(hostnamePort):
    domainName = validateUrl(hostnamePort)
    return jsonify({'domain':domainName})

def validateUrl(hostnamePort):
        for domain in urlInfoDB:
            if (domain['domain'] == hostnamePort):
                return domain

if __name__ == '__main__':
 app.run()
